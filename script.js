let camera, scene, renderer;
let controls;
let player;

init();

function init() {
    // Set up camera, scene, and renderer
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.set(0, 10, 30);

    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    // Set up controls
    controls = new THREE.PointerLockControls(camera, document.body);
    document.addEventListener('click', () => controls.lock());

    // Add player
    player = new Player();
    scene.add(player.mesh);

    // Create a simple terrain (you'll need to replace this with a more complex terrain)
    const terrain = new Terrain();
    scene.add(terrain.mesh);

    // Start rendering loop
    animate();
}

function animate() {
    requestAnimationFrame(animate);

    // Update player position and controls
    player.update();
    controls.update();

    renderer.render(scene, camera);
}

class Player {
    constructor() {
        this.geometry = new THREE.BoxGeometry(1, 2, 1);
        this.material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.position.set(0, 0, 0);
        this.velocity = new THREE.Vector3();
    }

    update() {
        // Update player position based on controls (you'll need to implement controls)
    }
}

class Terrain {
    constructor() {
        this.geometry = new THREE.PlaneGeometry(100, 100, 10, 10);
        this.material = new THREE.MeshBasicMaterial({ color: 0x00aa00, wireframe: true });
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.rotation.x = -Math.PI / 2;
        this.mesh.position.y = -1;
    }
}
